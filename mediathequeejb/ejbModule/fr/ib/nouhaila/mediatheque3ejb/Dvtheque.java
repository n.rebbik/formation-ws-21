package fr.ib.nouhaila.mediatheque3ejb;


import java.time.LocalDate;
import java.time.LocalTime;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
//non stateless : car il ya un attribut -stateful:on prend en consideration
@Stateful(name="Dvds",description="Operation pour la Dvtheque")
public class Dvtheque implements IDvtheque {
	private LocalTime derniereInterogation;
	public String getInfos() {
		return "Nouvelle Dvtheque, ouverte de 10 � 19H";
	}
	
    public boolean ouvertA(LocalTime t) {
         
    	derniereInterogation=t;
    	return t.isAfter(LocalTime.of(10, 0))&&
    			t.isBefore(LocalTime.of(18,0));
    }
    
    public LocalTime getDerniereInterogation() {
    	return derniereInterogation;
    }
}
	
