package fr.ib.nouhaila.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.nouhaila.mediatheque1cxf.serveur.ILivreService;
import fr.ib.nouhaila.mediatheque1cxf.serveur.Livre;
import fr.ib.nouhaila.mediatheque1cxf.serveur.LivreService;

public class ClientMain {
	
	public static void main(String[] args) {
		System.out.println("Client Livres ");
		JaxWsProxyFactoryBean factory =new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivreService.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivreService livreService =factory.create(ILivreService.class);
		System.out.println(livreService.getInfos());
		System.out.println("Livre 4 empruntable :"+livreService.estEmpruntable(4));
		try {
		System.out.println("Livre -3 empruntable :"+livreService.estEmpruntable(-3));
		}catch (Exception ex) {
			System.out.println("Exception : "+ex.getMessage());
		}
		System.out.println("Retour du livre 4" +livreService.getRetour(4));
	    System.out.println("Livre du mois est :"+livreService.getLivreDuMois());
	Livre[] la =livreService.getLivresDeLannee();
	System.out.println("Livre de mois Mars:" +la[2].getTitre());
	}

}
