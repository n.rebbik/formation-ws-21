
package fr.ib.nouhaila.mediatheque1cxf.serveur;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;


@WebService
public class LivreService implements ILivreService {
	public String getInfos() {
		return "L'entre de la bibliotheque se trouve avenue Victor Hugo";
	}

	public boolean estEmpruntable(int id) {
		if(id<1)
			throw new IllegalArgumentException("id doit etre 1 ou plus");
		return false;
	}
	public Date getRetour (int id ) {
		// envoyer toujour: ajourd'hui+10 jours 
		LocalDate d = LocalDate.now();
		d=d.plusDays(10);
		return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public Livre getLivreDuMois() {
		
		Livre l =new Livre("The power of now","Eckhart Tolle",1997);
		DataSource dataSource=new FileDataSource("image.jpg");
		DataHandler dataHandler = new DataHandler(dataSource);
		l.setImage(dataHandler);
		return l;}
	
	
	public Livre[] getLivresDeLannee() {
		Livre[] livres = new Livre[12];
		for (int i=0; i<12;i++) {
			livres[i]=getLivreDuMois();
		}
		return livres;
	}


}
