package fr.ib.nouhaila.mediatheque1cxf.serveur;

import java.time.LocalDate;
import java.util.Date;

import javax.jws.WebService;

@WebService
public interface ILivreService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee() ;
}
