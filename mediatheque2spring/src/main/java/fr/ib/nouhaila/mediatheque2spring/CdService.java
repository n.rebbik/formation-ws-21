package fr.ib.nouhaila.mediatheque2spring;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import fr.ib.nouhaila.mediatheque2spring.Cd;

// c'est le model et le model doit etre accessible depuis le controller 

@Service
public class CdService {
  
	private List<Cd> cds;

	public CdService() {
		cds =new ArrayList<Cd>();
	}
	
	public int getNombreCd() {
		
		return cds.size();
		
	}
	
	public void ajouteCd (Cd cd) {
		cds.add(cd);
	}
	
	public List<Cd> getCds(){
		return cds ;
		
		
	}
	public Cd getCd(int n) {
		return cds.get(n);
	}
	 public void changeTitre(int n,String titre) {
		 cds.get(n).setTitre(titre);
	 }
	
}
