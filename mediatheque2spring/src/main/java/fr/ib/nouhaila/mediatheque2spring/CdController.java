package fr.ib.nouhaila.mediatheque2spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class CdController {
// injection de service dans controller : declaration ==> method set 
//  ==> @autowired==>dans service ajouter @service
     
	private CdService cdService;

	public CdService getCdService() {
		return cdService;
	}
    
	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}
	 @RequestMapping(path="/cd/nombre",produces =MediaType.APPLICATION_ATOM_XML_VALUE)
     public int getNombre() {
    	 return cdService.getNombreCd();
     }
	 
	 @RequestMapping(path="/cd",method=RequestMethod.POST)
     public void ajoute ( @RequestBody Cd cd){
    	  cdService.ajouteCd(cd);
     }
	 @RequestMapping(path="/cd",method=RequestMethod.GET)
	 public List<Cd> getTous(){
		 return cdService.getCds();
		 
	 }
	 //num est compos� de 1 ou plusieur chiffre:\\d+
	 @RequestMapping(path="/cd/{num:\\d+}",method=RequestMethod.GET)
	 public Cd getUn(@PathVariable("num") int n){
		 return cdService.getCd(n);
		 //pathvariable pour dire que la valeur dans url est la valeur int n 
	 }
	 @RequestMapping(path="/cd/{num:\\d+}/titre",method=RequestMethod.PUT)
	 public void changTitre(@PathVariable("num") int n,@RequestBody String titre){
		  cdService.changeTitre(n, titre);
		  
	 }
	 @RequestMapping(path="json/cd",method=RequestMethod.GET,
			 produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Cd> getTousJson(){
		  return cdService.getCds();
		  
	 }
	 
}


// d'abord on cree la fonction dans service puis on l'injecte dans controller avec un requesMapping et dans clientmain on affiche 
