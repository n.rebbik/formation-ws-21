package fr.ib.nouhaila.mediatheque2spring.client;


import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.nouhaila.mediatheque2spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			
			System.out.println("Client Java Spring");
			RestTemplate rt =new RestTemplate();
			String health=rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : "+health);
			Integer nbCd=rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			
			System.out.println("Il y "+nbCd+" cd � la m�diatheque");
			Cd cd1 =new Cd("Abbey Road","The beatles",1996);
			rt.postForObject("http://localhost:9002/cd",cd1, Void.class);
			//ne fonctionne pas : 
			//List<Cd> cds =rt.getForObject("http://localhost:9002/cd", List<Cd>.class);
			// solution 1 : 
			/*
			ParameterizedTypeReference<List<Cd>> ref= 
					new ParameterizedTypeReference<List<Cd>>() {};
			ResponseEntity<List<Cd>> cdsEntity=rt.exchange("http://localhost:9002/cd", HttpMethod.GET,null,ref);
			List<Cd> cds= cdsEntity.getBody();
			*/
			//solution 2  : 
			Cd[] cds =rt.getForObject("http://localhost:9002/cd", Cd[].class);
			System.out.println("Tous les cd");
			for(Cd cd:cds) {
				System.out.println("-"+cd);
			}
			
			//parametre de requete : http://localhost:9002/cd/0
			Cd cd0 =rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("le premier Cd : "+cd0);
			
			
			rt.put("http://localhost:9002/cd/0/titre", "Help!",String.class);
			
		} catch (Exception ex) {
			
			System.out.println("Erreur :  "+ex);
		}
		
	
	}


	}


