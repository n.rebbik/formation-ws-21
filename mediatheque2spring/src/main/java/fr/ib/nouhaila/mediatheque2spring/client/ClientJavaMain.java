package fr.ib.nouhaila.mediatheque2spring.client;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java");
			URL url =new URL("http://localhost:9002/health");
			URLConnection conn =url.openConnection();
			InputStream is =conn.getInputStream();
			BufferedReader br =new BufferedReader(new InputStreamReader(is));
			String reponse =br.readLine();
			System.out.println("Health:" +reponse);
			br.close();
			
			
			
		}catch (Exception ex) {
			System.out.println("Erreur :  "+ex);
		}
		
	
	}

}
