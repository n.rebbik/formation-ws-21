package fr.ib.nouhaila.mediatheque3ejb;


import java.time.LocalTime;

import javax.ejb.Remote;
@Remote
public interface IDvtheque {
	
	public String getInfos();
	public boolean ouvertA(LocalTime T); 
	public LocalTime getDerniereInterogation();
}

