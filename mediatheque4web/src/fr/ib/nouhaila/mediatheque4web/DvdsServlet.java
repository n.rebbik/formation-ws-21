package fr.ib.nouhaila.mediatheque4web;

import java.io.IOException;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.nouhaila.mediatheque3ejb.IDvtheque;




@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private IDvtheque dvtheque;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    resp.setContentType("text/html");
	    java.io.Writer out =resp.getWriter();
	    out.write("<!DOCTYPE html><html><body>");
	    out.write("<h1>DVDTHEQUE</h1>");
	    out.write("<p>"+dvtheque.getInfos()+"</p>");
	    out.write("<p> Ouvert � 9h? "+dvtheque.ouvertA(LocalTime.of(9, 0))+"</p>");
	    out.write("<p> Quand ? "+dvtheque.getDerniereInterogation()+"</p>");
	    out.write("</html></body>");
	}

	public IDvtheque getDvtheque() {
		return dvtheque;
	}

	@EJB(lookup="ejb:/Mediatheque3EJB/Dvds!fr.ib.nouhaila.mediatheque3ejb.IDvtheque")
	public void setDvtheque(IDvtheque dvtheque) {
		this.dvtheque = dvtheque;
	}


}
