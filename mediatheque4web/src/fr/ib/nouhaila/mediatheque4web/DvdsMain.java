package fr.ib.nouhaila.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.nouhaila.mediatheque3ejb.IDvtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
        try {
		Context context =new InitialContext();
	    IDvtheque dvtheque =(IDvtheque) context.lookup(
	    		"ejb:/Mediatheque3EJB/Dvds!fr.ib.nouhaila.mediatheque3ejb.IDvtheque");
	    
		System.out.println("Information : " +dvtheque.getInfos());
		System.out.println("Ouvert � 17h30?"+dvtheque.ouvertA(LocalTime.of(17, 30)));
		
        context.close();
	}catch(Exception ex) {
		System.err.println(ex);
		ex.printStackTrace();
	}
	}
}


